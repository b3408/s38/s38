//console.log('hello from js')

//DOM selector

//first you have to describee the location where in its going to access elements.
//identifiy the eattribute and the value to properly recognize the and identify which element to target//
//visualize the statement in JS
//let/const jsObject = {}
// const firstName = document.querySelector('#firstName');
// const lastName = document.querySelector('#lastName')
//getElementById can be esseential when targeting single element
const firstName = document.getElementById('firstName')
const lastName = document.getElementById('firstName')

//getElementByClassName => can be essential when targeting multiple component at the same time
const input = document.getElementsByClassName('form-control')

//getElementByTagName can be useed when target elements of the esame tag
const heading = document.getElementsByTagName('h3')
//check if you were able to successfully target an element from the document
console.log(firstName)
console.log(lastName)
console.log(input)
console.log(heading)

//check the typee of data that we targeted from the document.
// console.log(typeof firstName);

//